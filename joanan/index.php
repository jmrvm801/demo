<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="css/materialize.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/perzonalizados.js"></script>

    <link rel="stylesheet" href="css/style.css">


    <title>Trapos Industriales</title>
</head>

<body>
<!--=================================================================================================================== 
             NavBar
====================================================================================================================-->
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper white">
                <a href="#!" class="brand-logo black-text text-black"><marquee>Venta de Trapo Industrial</marquee></a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger black-text text-black"><i class="material-icons">menu</i></a>                
                <ul class="right hide-on-med-and-down">
                    <li><a class="waves-effect waves-light btn blue"><i class="material-icons right">call</i>LLAMA AHORA</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <ul class="sidenav" id="mobile-demo">
        <li><a href="#!" class="brand-logo black-text text-black"><marquee>Venta de Trapo Industrial</marquee></a></li>
        <li><a class="waves-effect waves-light btn blue"><i class="material-icons right">call</i>LLAMA AHORA</a></li>
    </ul>
<!--=================================================================================================================== 
             Slider
====================================================================================================================-->
  <div class="carousel carousel-slider wrap-slider">
        <div class="carousel-item white-text item">
            <div class="center-align text">
                <h2>Trapo industrial para tu negocio</h2>
                <p>Industrias, talleres, laboratorios, y más...</p>
            </div>
            <img src="img/sliders/slider_01.jpg">
        </div>
        <div class="carousel-item white-text item">
            <div class="center-align text">
                <h2>Envíos a toda la República Mexicana</h2>
                <p>Envíos a partir de 50KG a todo México</p>
            </div>
            <img src="img/sliders/slider_02.jpg">
        </div>
        <div class="carousel-item white-text">
            <img src="img/sliders/slider_03.jpg">
        </div>
        <div class="carousel-item white-text item">
            <div class="center-align text">
                <h2>¡Llámanos ahora para realizar tu envío!</h2>
                <p>CDMX: 55 32 40 24 79 | gonzalezc@roqueint.com.mx</p>
            </div>
            <img src="img/sliders/slider_04.jpg">
        </div>
  </div>

<!--=================================================================================================================== 
             Productos
====================================================================================================================-->
    <div class="container body">

        <h4 class="center-align">Contamos con disponibilidad inmediata</h4>
        <div class="blue darken-4 separador"></div>

        <div class="row">
            <div class="col s12 m4 l3 center-align">
                <h5>Filtra por condición</h5>
                <div class="row filtros">
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">NUEVO</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">USADO</a>
                </div>
                <h5>Filtra por color</h5>
                <div class="row filtros">
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">BLANCO</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">COLOR</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">PINTO</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">LISO</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m11">MULTICOLOR</a>
                </div>
                <h5>Filtra por tamaño</h5>
                <div class="row filtros">
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">GRANDE</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">MEDIANO</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m11">CHICO</a>
                </div>
                <h5>Filtra por tipo</h5>
                <div class="row filtros">
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">CAMISETA</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">SÁBANA</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">TOALLA</a>
                    <a class="grey-text text-darken-2 waves-effect waves-light btn  blue-grey lighten-3 col s12 m5">SUDADERA</a>
                </div>
            </div>

            <div class="col s12 m8 l9">
                <h5 class="center-align">Nuestros trapos industriales</h5>
                <div class="row">

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://sites.google.com/site/laventadeservicios/_/rsrc/1444376286863/home/1-analisis-del-producto-industrial/1-1-clasificacion-de-productos-industr/tornillos.jpg?height=307&width=480">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 7<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Varias medidas</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 7<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhUSEhIVFRUXFRcVFxYXFxYVFxYVFRcWFhcXGBgYHSggGBolHRUXITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGy0mICUtLS0tLS0tLS0tLS0tLS0tLS8tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMQBAQMBEQACEQEDEQH/xAAcAAACAwEBAQEAAAAAAAAAAAAAAQIDBQQGBwj/xABAEAABAwEGAgcGBAQGAgMAAAABAAIRAwQFEiExQVFhBhMicYGRsTJSocHR8AdCcuEUM2KCFSOiwtLxkrIWQ1P/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAgQBAwUGB//EADURAAIBAwIEAgkDBQEBAQAAAAABAgMEESExBRJBURNhBiIycYGRobHRFSPhFDNSwfBCohb/2gAMAwEAAhEDEQA/APuKAEAIAQAgIuz0QDagGgBACAEAIAQCKAAgGgBACAEAIAQEXAoBhANACAEAIAQAgIkICQQAgBACAEAIAQCQDQChAEoBoAQAgBAJANAIhAEoBoAQAgBAJANAKEAAoBoAQAgBAJANAKEAAoBoBIBoAQCQDQAgBACASAaARKAiAgJoAQAgBAJANACASAaAEAIAKAUoBoCLigBoQEkAIAQCIQBmgGgEUAIBoAQAgBACAEBElADWoBuMao3gGfab6pM3xHl9VTq39Gn1ybY0ZMzqvSJ35aY8ST6KjLi8f/KNqtu7Kv8A5BV91vkfqori77Ev6ZFtLpE78zB4SFuhxWm/aRF2z6M7KXSCifalvgT6KwuI276mt0JnTRvSi4w14JOgzHqt8LmlN4jJEHTkt0di3kBoAQAgBACAEBAuQDa1ASQAgBACAEAIBEoACAEA0AIAQAgAoCJcgANQHFeF5NpZe073Rt38FTub2nQWu/Y2QpORgWu2VKvtHLgNF565v6tV74XZFyFKMTn6tc9zNpIU1jmYH1akmzIGlyU9QQLBwn4ImCxlkY7RxaeB+oW+CUtpYfmReVubN229zSKVU5/ldx5Hmu/Z3b0p1d+j7lSrSXtRNldMrAgBACARKARKAbWoAQDQAgBACAEAIBAIBoAQChAAKAaAEAigABAZt63jgGFh7W59391zb6/jRXLF+t9jdSpOWr2MIMnM+fH6rzE6jm+aTLqWNgDOC05ySFWxBpLW4nbNBAnxOg8+5ZSWdXhGSTGuLO1DHEZ4TiwnkXNz8QmUnp9QSs1PAIxPdnMuOf0HcApqff6aBkaFFjCS1pBJknETJz496k6ieG8/P+BqX9aOfqpqrHrn5kcMRYw8vh+yl+3LZ/6M6obhlhfps7cHYrdGUkuWe3R9iLXVGxdNqL2lrvbZk7mNnDvXorK48WGJe0t/yUasOV5WzO8FXDUNACARQAAgGgBAJANACAEAkAQgGgBACAEAIBIAQDQHHelpwMJGpyH1VS9r+DRcupOnHmlg881m5Xj229WdElEqG4KrZYW1Whr5wzJaCRj5OjVvLRbI80dtPv8AAymkXfBamhkIRIBCzgBhWTIi1ZwBFqAG1COY4LZGo46dA0W063VvZUGg7Lv0HWe45roW9fwqkZ9Nvh/BqnDmi0emXqDnBKAEA0AIAQAgBAJAEoAQDQAgBACAEAIAQChAAQDQGDflWXhnAfE/YXn+M1dY00W7eOmTjK4W7LJaweew4d63wjql1Itl4pwuhGgoe81uWRPaN1GpTjJaoJs5nNXNeE9DchhYSyZLG0lap2+dyLkT6tWlQSWEiHMUVGQqNWm4PU2p5KnBaGSCmJlp3HxW+j62Yd9veRlpqb1zV8VFpOoGE97cvSF6qxq+JQjJnOrR5ZtHaQrZrAIBoAQAgBACARKARbugGCgGgBACAEAIBSgBANACASA8xa34qzj/AFR5ZfJeT4lLmuGX6KxATTCoJ8qybS+zDdXLKOrmzXUfQvldDmRrK6zslVuqijHC6k4LUoXNZtBZRk6Kei61DDimaZaMtVvTBAotJyVK8w45NlPc5CuUzeKme0O9To554vzMS2J3Xego1erf7L3kA+646eBhdawvVSn4Utm38GaK1FzXMt0epXoigCASAaAEAIBEoAAQDQBCAUoBoBIBhARLkAg1ATQAgBACA8i49t36j6leNvX+/J+Z0afsolKpPcmX06gAV2jWhGGMmtxbZLrQtjuYjkZW50qnObk8mxLApUDI5WUCym9XrephYISRPGrPi6EeUpqPVWtVTWDZGJSVVUe5LI7Pm7kM1ut3mfktTEloYV8iWk88Xiq0pZ3N0Nz312Vi+jTedXMaT3kCV7W2m50YyfVI49SPLNpdzqW8gCASAaARKAAgGgBACAEAoQDQEHOQDaEBJACAEBGpUDQSTAGZKxKSisvYJZPP2293vOGkCG8dz9AuRWv5SfLSRZjSS1kcDZxGdZXCu8+I87lmGxMqpjG5IbVlLJksAW5RSIjlZALGEZFKaGRh62qaSGCJctUp5JJESUWEssESVCUm2ZSwMuhpG59FthNQptLd/Yw1lmReTZaGDVxjuGpPgFVzmSRujpqz01hv6y07HStDqzG0sAAcTqRkWgauIIIgScl7u3g4UoxfRI4tR802zlujp9YbQ/A2qWEmG9YMIcdBDpIBOwdBOwW4jhnqEMAgIoBhANACAEAIAQAgIFyAkAgCEASgGgBAYF9W9pdggnDqJgYuZ1K4l/f04y8PGcfBFqjSeOYzv4p20NHBoie86rlTv6ktI+quyN6prqVAyZVSdRzbbJpYOwNxiRqNQtnhqrDMd1uYzysrJha8qOhncYesqWQOVnIASdFjV7AT2xrkoS9X2iS12Itqt2l3csQnFvGrJYZa1w4Ad6tRaisuKXvNby9it9UcfJaalTPUnGJU6rwWnmJ8pW+r96qLn2JKJ4zprfwoh1Jhms8Rl/8AVTP+4/egns8IsHUn4s1ovq/wV7mtyrlR87t1OoGNOIkCRhk9kHMkDbPVesaOcUWclpxTHxy3BG45KIPqfQL8R+rw0LUS6nkA/Nzqfzezl7TeY9kYaPrFht9Ku3HRqMqN95jg4eYWCJ0oBIBhACAEAIBEoBYkAwEA0AIAQClANAeOtI/zHn+t3qV4e5f7035v7nSh7KIgKu2TAtWMgdN5BkJTm4yyg1lHZTq4tWzzCvQqqqvWjk1uONmPq2d3mFPkoe75jMgDqY3HqsrwImPWZF9sA0HyWJXMV7KMqD6nLVrl2w9fVUalRSeWboxwQxk7rV48Y7E+URP3ooOcpapEkkRdUHFZUZksEDUHEKLjN6EsGDfV7Pgss4OP/wDQsLmt4wJGI/Dv0XQtLSKfNV27EZptYR5Cy9Eqz3F73Pe5xkuLAJJ1JLnr0S4nCEcRitPP+Cm7PLy5Gm/oC54/nlp/SD6OUP1eX+H1Iu0XcrZ+HDfzWoxypgf7kfFX0j9QrRdy6l+HVJufXvJ2ljSBziYPjIWP1WX+KH9Iu52UOi76M1Kdse1zQTjwwQNSC5jgS3TsqUOKNyw4mJWqS3Oro7+KNVmFtrAqsJjrWw13jo2eRDcs5OS65ScT6ldt4UrRTFSi8PYdxseBGoPI5oROmEASgGgBAKEAQgGgBACAEAICm01202Oe4w1rS4ngGiT8AsN4WTKWXg8ZZbc20N65gLWvc4gOycO0ciF4q9jy15ebz8zpQWEkdICpkhrGDIdXKzGOWYyRc5zMp58VszOg8ZGktROtLiIWJV5NaklFCawnZQTfRGCu1VmUml1R7WgCSSRkOZOQ8VmNGrUfKvyzPNGOp4q9PxHs7CW0WPrEbgYWz3u+QK7Fv6OVqms9Pv8AQrzvILYxbR03trwC3qqDSJ9l1R8SQCJhp0O2y9HZ+iVNpNs59bivK8JFVW/y4gvNao3cOq9WHf20xAC78eAUowcaajF91Hmf1KP6i+fMsvyzj7HoLl6R0KsU4FE5BoMYOQDsvIgLy3EvRm5t81YPxF1xuvevwd204tRq4jJcr+nzN6nTgxM5Z/Lu7l5txOvkuxEcPRZMaCcXcGjvk+kLKeBhBEanyEfVSU5dyOI9gwiCXOLQNy6ABxOy20oyqS5YrL8lqa6nLFczeEcda+7PTEurNdOmuf6QBmNvmrFWyq05cs4tPsyFOrCosweUZ179IXvpxZmgEfmJ0J3DdCe8+BWyhbQUv3DNWnU5cwPmwc6m4hw5EGYMajjPxHJd9NNaHHaaep6Top0oq2Gq19NxdSdq06OAyLXRoRsdRkRkSHZIs++3PetK1Um1qLsTXd0g7tMbj7yWCJ2oBSgBANACAEAIDmvC3U6FM1argxjdSfQAZk8gsNpLLMpNvCPCW78TO1FGhLeNQwT/AGtmB4rQ6/ZFiNv3Zyt/EWu78tJvLC4/HF8lplcTXQ2q2h3Zm3z0gtFYf5lQ4fdEBnkNfGVplWlPdm6NCMNkHRW+Gh5ouMYzLf1bjxHpzXKv6DlHnXQmz2IXGIkgsguotVy3ppvLISZY9gOoBVqcIS9pEE2jhtlelSBe8tY0buMD46rnqlzyxTj9zam+54m/vxCgEWZsxl1j5A/tbqfGF1Lfhkn/AHX8DDemTwVtv602gFlVwLXGC6ML8OIOwyMgJA0GcZleysOBRp4qNYfb/upxbm/zmMTpsthYajGEZOcGkj+ox4d678qUacW0jkKtObw2UspVKxx4HOxHshrTGQgAcgAB4Ld4lOlHVpJGXGUnoiVtu2tS/mU3tGsxIA7xkpW97b1v7c0/cxO3q0/ai0cXorq1NZp3X0hr2fJjsTfcfLm+GYI8CuRxDglpeazjh91o/wCS/bcRr0NIvK7PU1HdNrRtTpeT/wDmuSvRK0xq5fNfgtPjlfO0fr+SNTpraDm1lMHucf8AdksQ9ErNP1nJ/FL/AEZlxyu9kl8yDemVqmIp9+Ez/wCy3f8A5WxztL5/wQ/WrjHT5HTab6qWhjRUaIE5NLmtJB9o5yTtrx4qhd21HhXqW2eaXV7pdk/N9S9ZSqX/AK1derHp0b8/ciVWxdY1pBwYQYADcOcTI301mVwXUbep31SSWmhKxsIMRDpyjNruQ4HXsnwlRepJaaMuvG6qdVsub3xk5vCOXpPMhTpVpU2aq9vCqtd+54+22R1Bxa8TTd7LhvwIPvD4g+XTp1IzWUcarSlTliR6DoV0tq3fVz7dJ0YmTk4bObwMaeRjaZqPvt3XhTr021aTg5jhIPqDwI4LBE6YQAgGgBAEoAQHzL8VrY51WlQnstZ1h/U4uaPINP8A5KrXlrgtW8dGzxbaGS0Fk53kA6pgZJMvCMvMbHu5/fdGVLOpmNTG5SbI55xexnuTPkNFDDSwybeT2dyXzUa3DVqsq7AwWP8AEnJ3kuVcWUW8wWPsZUc9TY/xtoH8t57i35kKtG0edR4bOav0oqDKnZ4/W+P/AFlXIUmljKX1/Bjwe5m2u+La8EdYykP6GyR4u+i2qlHrl/Ql4SRjPu5zu1Xe6oRo4kk58Pd8FbglFeqsEWlsYV+MYzsgf98T+67nBbR16viy9mP1ZzOJ3Cpw8OO7+3Ux7vgh7yJzgcJ39QvX03zNs85W0xE76NZzS1/5pDhyIILcvD4qThlamlNJ6dD3d3XnSwUAxjWktDWtzyw9kwQCdtT9V804lw26VatN5cY65fZ7f8j2lleUHTpx2lLRJd1ubLnh3tN+Ykc1xIScXlPHuOm45WNzBv7o3SqgvaIeTkRv4b6b8NRqvScL9I69s1Gq+aP1OXd8Jp1k3DSX0PE3pc9Wz/zG5e8Mx48PRe9seKW95HNKWvbqeZuLOrQfrozHOCvSeCuky1mixEg9x0G5nksr2jE3oeia3AxreA+O6+fcYr+NeTfbT5fye34VS8K0j56/M6LFa4yOYOS5ckdKLNCnZ/LcFa2yeC3rWgEOM6DvaZEHyWNTK7GdWa14dTqAvBzE7jOYPvDI8c+S205Si8xNVWnCa5ZHmrxul1EQSHNJ7DjEz7juZg+I2kx06VVTXmcWvbypPyNr8PumL7DWDXEmi8gPb8AWj3h8dDsW7cFc/QNltDKjG1Kbg5jgHNcMwQcwQsES1AJAEoAIQBKA+EXxbH17RVqFxMvdAM5Nk4RyAAVCcsvUvQWIpFNQOaJwn1WME8mDeFo4KaRFs7bgoYm9YdzDeQGRP3wUJvoTh3NR9E6/fluo4JZKiVjBk6rJb3DKZHA/LeeS1Top7E41GtzUo2gOGWZ4bqu6bTNyaaLpDTJI47j7KmoYIOWTLt9twgkGO5bYpt4ISaSPE31aS7E/f5fcr3trQ/p7RRW+Mv3s8lXq+NcOXTZHLcZJaS7NsmB/V9FYssyi2V7zCaS3NJxzz1Kuyw9CnHJ6foPdsufXOjeyObiM/AA/6ivH+ld54dNWsd5av3LZfE9FwK255OvLZaL39T2b25Tv6rwWD1SepnXXaW1HVA0zgfhy0EgEjlnstk4OKWeoymzsq0mOEOAPep0qs6UuaDwyM4KaxJZR5u09DbO58jE0TJAOXdy8IXoKXpTdwhyySfmcupwahJ5TaLR0Ms0fm02cUXpVe9o/X8kP0W38zgvborSoU2vY55Je1pBIIO5jKdAV2eF+kdW5nKNSKSUW8ryKN5winBR5G8tpY9/4Kf4EOgz4fsvOSqNybfU9RGmkkl0LSG09GlzuHnx7io4bMuUYlFR9d5yLWDgJPnlms4jEw3KROy3eMeIuJJMkwe7SY+Cy5trBhU1F8x3OpsaCJzzykzyI4d6h7OpP29DlqgVJa8B2IREdl418Hb/FZUnnK3IuCa5ZbHlb8us0zjGbCYnUg/1czx3IneF0qFdVFh7nHubZ0nnofQfwd6Vlr/4Kq7suJNImcnnMjkHeHajUuW9lRn2FYMAgBACAqrzhOHWDHM7ID85V6tahULKgcx4PaZUGff3a5jVaJU0yxGo0WOvoRDmkcxmPqtfhPoT8VdTKtNem8w1wkmBtmU5ZLccyex6KwUHU2NYAXRvlnJJ08Vob5nk3r1Vg7A/w78imDOUc9oIOaGTjc7Yd30UwzqpWrCASRi2Ij1jT9wjiupBSeS1t64wdiNtuSruGGWFLKMG9baSYndeg4NYqpLxZ7L7/AMHG4ndOMfDju/sYlpJcQ0auMAL09Z/+V1OJSSS5n0NGlRDQGt0GQ+ZPqrFOCpx5UU5zc3zMhZZfVfhBMEU2Di4/PMea0xqRUpzk9I/8za6bcYwW8j6xdNgFCiykDmB2jxcc3HzPlC+U8SvHd3M6z6vT3dD3dnQVCjGn2+/U63uyVJLLLJjdEaAFmFTIms99YniajiR/pwjwVq7l+649sL5GmkvVz31Oi8bxbZ6RqPEgOaD/AHuDR8XBRtreVxVVOG7JVqsaUHOWyOoBrgC3cT4LTNYeGbIvKyTaFAHnuk9sD6ootdBYMUDMlxifJpHg48F6K0peDZ8/Wo//AJj+X9ihnxLnHSH3f4X3MhlRxEY3EfewWnQ6GuxNpDdvHdYbySSSKX2qOHHM7BFHIcsE+27KQ0b6SdtI5ehzU24xNK55+RbZaLWSQS5x1c7MzrlsPBapSbN8IJE6kGZ0PmDxHPmoksCNPGC14BkGZ0e3jycPopKTTytyEoprllseStFF1krtIJ7JD2OGRifIOHrBXWo1VUjk4dei6U+Vn6R6NXs21WanXaQcTc4y7QydkcwJEjkQplZmmgBAJABCAxr+uSjaW4a1NrxtIzHNp1aeYQHzi+vw2YJ6l72cj22688/isYM5PFWzovWoVGucWua1wJ1bkCOSxJaEovU1aN6sBzkfEfBU/Ckti26kWaQvCm8ZOafEKLTW6JLHRnFVptJAgZrCZJnLbCyn7JcHa5TwkYp2z0W1YIPLM2lTqvnhlDnENB8XHPwUuTJHnSO7/CatNpfUc2mCIJdLR3y4D0WHSyRVbBUyzWLAXPtGLZ2EF2u2WR1XfsKtdUfDpw0WdWzlXUKTq805amc6lZ5D6Ln1CJYcTC2NNMzJgrr2niTk51MabHOunFJQh11YUakzIOhOmoGZ9Fecim4bYC7C+k5rmkNc0l06y85nXhMf9Ks7dVKbpTWj388m91nGanHfp5JHrrq6aHSuyR77B6t+nkvL3/onH2raWPJ7fM7dtxxrSsvijq6UX3TNjquo1AS4YBGoc/siQcwc1wLfhNzTuowqwaWfg/idapf0ZUXKEs/cq6OdIKZbSs2EsLWta0kyHYBHgSM/sLocX9HqtGMriEuZZy1jVfwVuH8VhVapSWH0fRmf00vgPriygdmnFSptJIGBs/3YvALd6N2GW677PH5+Zq4zdYiqS7rP4NCj0yoEQ6m9kcIc0cOB+C13HolXTbp1E/flMnQ47TaxOLXu1N267yo1v5dRpMTGhjjBzhcO44TeW39yDWeu6+aOjSvqFb2JI8PbnGq51UHMvc4EjQkkt8MJA812LxqM1TW0Eo/Lf65I2UM0+frJuXz2+h2WOoKjS+AIIDmyJneBGQ7gdlVcY4zktKcuZLGh00nNcYw7a5k/Ba9DY8ie+CQ0wZ2yHPxzP3plyxojChl5ZUJ1KgbiwMJ0B79tt9Nx5phjmSIim6CciBrBBImNQPJS5PVzk1ur6yjjfqToDCRmdQQeB2K1M2Y6HJfjHFk0h2gQYhpykhwl8gDeeSs21VQlrsypd0nOGm6Ppn4UWOtTsRdWdLqlVzwMjhaGtpj2cs8BOXELpKSkso4s4uMsM9mVkgLNANACARCAorUAUB52/bgZVY5pGREZZHwKA+Z3p0LrUyTTdiHB2R8xr5BYwS5jAtV21me3Rd3gYsh+lRwSUkZrar2buaeEkRwkHdYcUzKk11PZ0rJSbSZVLC8OAMuMuhwBEkQBE6gKcaUTDqNnJe98GlTx0WNonHhxg43kYZycQIM7jzWxxSI8zPD3hb+sJLgXSZ7Ti4+Z9FFt9B7ziNoOgynb0C20as6afK8ZIVIRnutjY6POllRsw6ZngHN1/wBJHiu5wqrmEk3rn/RyOIQSnGWNMf7O5tSeyDtBd3bDu+i67a2RQ5cesyMg5DQaKWxh56kmhCLOaqMVRrdcOZ8P3+arSanNIswTjTb7lld+fdOfkrOE9yMcrYqsdpNarVqvOIuLRPJjcI+S5/D6MKbkqaws6fcsX1SUlHmepeTsus0UlsXAAQZj7z+EqtXkqVOU3sk2bKEHUqKHd4NeyEEYtREO/TxHd96L5lObcm31Po0IpJY6FuDqn49Ro8aAt4/vw7lHPQk11O8sbriEZxx5ZDSeajjzMpvsVmowHIE9+U58By5plGdSQc/YBnMw0/lzzg+6fjxWdTHq+8TaRmcXDQOnYcBtz2UdOrM57ImygXe+eUD/AJLKjnYxKpyrLwjao3C+qQQ0UwGhvEmBEmIzVv8Ap5Txpgof1sabevNk9XcXRWjSIe4dY8buAwgzMhvHmZIW+nbxhruVK13Oppsj1QW8qAAgGgBACAEAoQEXMlAcNosLTsgOCtcjTsgMK9+jLHA9kHvEoDz76VekQxjGlrW4cOGBhGQnbbWJyU0DKvq5HV2kCm1smm/CCYBAqNfEnLPDlpmFgZMZvQ124pjvz+SzgZLOjlytqf5pdga15aAABIaBnPipIxkvvG66DHEBgh0OLvZJ45iN11bOWI6blK4Sb1C9bvp0qVIUg0YmGo4QXe7hM85d5KzaXWaklJ9dCpd0MRUkveYLmkHgRwzC62Tnkar4Guew4nZaK9eNKLlJmyjSlUmopFdmomn2nDNwn6ep81qtG5wU31+xYuMKXKuhzVg5wIbJc4wANSTorNefLCTMUo5kkajLrNnY1p1Op/qylc/hNbxHP3onxGHK4/E5XBdwpp6DdAa4nQAz4Zrjcaq8tDw/8vsX+Gp+Lz9jZ6O1mvph2LXPwXga0WpHuKFRSjlGoKOJsSARpO44aZnb7y1Jm3YhTogRnIHgBptmY14I2jKyWYwPZ+nHWM9+Kwm3og0t2dFisdSoeyA0cQM1YhazlqypUvaUNFqz0l39Gd3EuPP9lahawW+pRnf1ZbaHo7HcbW7KwklsU5Scnls1qFiA2WTB1NEZAICwBANACAEAIAQAgEUAQgEWoCupRBQGRbroDsxkeKZBj2q6nAGAT4/tyHks5BwG7jnII7wTPkCmQcLbua0yKTjyAgCdctJUuYxgLVdlSs3B1TQ3i4B5HcNAfPuU4VXF5RGUEydLoyTGKTzPn8ytcpOTyySWFghU6B03HF2m8mmB5K7DiVeEeXOfeVJ2NKTzg6bN0KpsENb4nMnxKrVq9Ss8zZYpUoU1iJ4bp5dBs9oDYOFzWuB23BjxHxXoOGVOagl20OVeR5aue5ldFbM6pbKLGD84JymGtkmfAEeK3X8kqEsi2WaiwfSL86LCuyAcLgZaY34Hl9F5+yvHbTzjKe50Lm3VaOOqPnl63PXoPw1abpJ7JAlr/wBJGvdqvV0b2lWhzRfzONOhUpvlaNV/RKoaDmO7LntM5ThkZeS8zxK7VxVzHZae869nRdOn627OLo70TtNmcQ54ezUCCCD9D8lx69LnWm51rW48N4lsbNexV9erLv0kD1gBVVaTLv6hTS0yY1tp3lP+TYxHFz2uP/iCPmt0bRdWaJ8Qk/ZRqdB7kt9Wo42ykWNaIbkAHFxnQHOANee6sRpwgtEVKladT2mfVbsudrBopmk2aVnAQFwagGgGgEgGgBAKUA0AFARLkBIIAQAgEQgCEBB1IFAVOsjTsgK/4FvBASbY2jZATFmHBASFAIB9SEBxXnctntAAr0mVMJkYhMcYPgFsp1qlN5g2iE6cZ+0skqF206bcNOmxjRoGNDR5BRlOUnmTySUUtEiX8IFEyH8IDsgKX3Y07ICv/CG8EBIXS3ggJMupo2CA6qVlaNkB0NagJIBIBoAQAgFCAJQDQClAR1QEgEA0AIAQAgBAJACAaAEAIAQAgBARJQCiUBIBANAEIAQAgFCAJQAgGgBACAEAIAQAgKygJgIBoAQAgBACAEAoQAEA0AIAQAgBABQFYQEwgGgBACAEAIAQChANACAEAIAQAgBAf//Z">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 6<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Varias medidas</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 6<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://www.provesicsa.com/wp-content/uploads/2019/08/BODEGON-2019@1x.png">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 5<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Varias medidas</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 5<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://seguridadindustrialperu.files.wordpress.com/2014/04/equipo-de-seguridad-aislado-en-blanco-17013254.jpg">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 4<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Varias medidas</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 4<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://i.pinimg.com/originals/2b/b5/17/2bb5177882820b9d453138340f2a18cd.png">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 3<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Proteccion para la cabeza</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 3<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwcWiUdoUrXYCOXB3v854-UffMUvDGA8H8DAzTqyTVAF-WDBDO">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 2<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Chaleco fosforecente</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 2<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m6">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator img-card" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMJFouXSHUp3b0oZFhzR9a6-q_GcMVJeVvfp3I5XaiZZGE3dDg">
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Trapo 1<i class="material-icons right">more_vert</i></span>
                                <p><a href="#"> <marquee>Varias medidas</marquee> </a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Trapo 1<i class="material-icons right">close</i></span>
                                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Illum voluptate unde dicta consequuntur voluptas aspernatur dolore autem id quas. Facere asperiores ducimus consequatur officiis veritatis enim eligendi ad repellendus hic.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <h4 class="center-align">Envíos a toda la República Mexicana</h4>
        <div class="blue darken-4 separador"></div>

        <p class="center-align info">Contamos con pacas de 25 Kg listos para enviarte a toda México a partir de 50Kg. Todos los trapos industriales que vendemos están en condiciones higíenicas excelentes para diversos usos como industria automotríz, electrónica, alimentos, entre otros.</p>

        <h4 class="center-align">Pago seguro</h4>
        <div class="blue darken-4 separador"></div>

        <p class="center-align info">Te ofrecemos asesoría y consejos para adquirir tus trapos industriales. Todas las ventas se realizan por transferencia electrónica y factura fiscal CFDI 3.3 para mayor protección. Además, contamos con un candado de seguridad SSL para garantizarte la formalidad de este sitio web.</p>

        <h4 class="center-align">Contáctate con nostros ahora</h4>
        <div class="blue darken-4 separador"></div>

        <p class="center-align info">Ponemos a tu disposición el teléfono de la ciudad de México: 55 32 40 24 79 y el correo electrónico gonzalezc@roqueint.com.mx</p>
    </div>
    <br>
<!--=================================================================================================================== 
             Footer
====================================================================================================================-->

    <footer class="page-footer light-blue lighten-1">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Venta de trapo industrial</h5>
                    <p class="grey-text text-lighten-4">Carlos González</p>
                    <p class="grey-text text-lighten-4">Teléfono con whatsapp: 55 3240 2479</p>
                    <p class="grey-text text-lighten-4">gonzalezc@roqueint.com.mx</p>
                </div>
            </div>
        </div>

        <div class="footer-copyright">
            <div class="container">
                © 2019 Carlos González
                <a class="grey-text text-lighten-4 right" href="#!">Hecho por Joanan</a>
            </div>
        </div>
    </footer>

</body>

</html>