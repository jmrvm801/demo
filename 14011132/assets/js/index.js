/* globals Core, M, Form, ui, Fecha, Ax, u, Ic, Dropdown, z */
var trigger = 0;
$(document).ready(function(){
    $('.slider').slider({
        indicators: false
    });
    $('.modal').modal();
    trigger = $('.slider').height() + $('.productos > h5').height() + $('.productos > .borderstrix').height() + 44;
    if ($(window).width() < 600){
        $('.sticky').addClass('mobile');
        $('.windowx').fadeIn(10);
    } else {
        $('.sticky').removeClass('mobile');
        $('.windowx').fadeOut(10);
    }
	$('.materialboxed').materialbox();
});

u.click('.btn.unactive', function(v){
    v.removeClass('unactive').addClass('active');
});
u.click('.btn.cmdoor', function(v){
    setTimeout(function(){
        let coincidencias = [];
        $('.btn.active').each(function(){
            coincidencias.push($(this).attr('target'));
        });
        if (coincidencias.length == 0){
            $('.trapox').fadeIn(100);
            return false;
        }
        $('.trapox').fadeOut(100);
        $('.trapox').each(function(){
            let v = $(this), cont = true;
            $.each(coincidencias, function(i, e){
                if (v.attr('data-search').indexOf(e) == -1)
                    cont = false;
            });
            if (cont)
                v.fadeIn(100);
        });
    }, 250);
});

u.click('.btn.active', function(v){
    v.removeClass('active').addClass('unactive');
});
u.click('.sendbutton', function(v){
    var v = [
        $('#nombre').val(),
        $('#telefono').val(),
        $('#email').val(),
        $('#smotivocorreo').val()
    ];
    if (v[0] == '' || v[2] == '' || v[3] == ''){
        Core.toast('Completa todos tus datos para ponernos en contacto contigo. El teléfono es opcional','red');
        return false;
    }
    $.post('send.php',{v}, function(){
        Core.toast('Correo electrónico enviado. Recibirás una respuesta en menos de 24 horas.','green');
        $('.modal-close').click();
        $('#nombre').val('');
        $('#telefono').val('');
        $('#email').val('');
        $('#smotivocorreo').val('');
    });
});

u.click('.topper', function(){
    $('.sticky').toggleClass('notOpen');
    if ($('.sticky').hasClass('notOpen')){
        $('.topper').html('Aplicar filtros');
        $('.backgroundm').fadeOut(100);
    }else{
        $('.backgroundm').fadeIn(100);
        $('.topper').html('Cerrar ventana');
    }
});
u.click('.callNow', function(){
    
});

$(window).on('scroll',function(){
	var st = $(this).scrollTop();
    var sticker = $('.sticky');
    if ($(window).width() < 600){
        $('.sticky').addClass('mobile');
        $('.windowx').fadeIn(10);
    }else{
        $('.sticky').removeClass('mobile');
        $('.windowx').fadeOut(10);
    }
    if (sticker.hasClass('mobile')){
        if (st > trigger){
            if (!sticker.is(':visible'))
                sticker.show(100);
        } else {
            if (sticker.is(':visible'))
                sticker.hide(100);
        }
    } else {
        if (st > trigger && st < trigger + $('.card-group').height() - sticker.height()){
            if (!sticker.hasClass('active')){
                sticker.css('width', sticker.width()+'px');
                sticker.addClass('active');
            }
        } else {
            if (sticker.hasClass('active'))
                sticker.removeClass('active');
        }
    }
});